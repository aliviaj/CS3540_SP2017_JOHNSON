/*
 * Sampling.h
 *
 *  Created on: April 26, 2017
 *      Author: aliviaj
 */

#ifndef SAMPLING_H_
#define SAMPLING_H_

#ifndef _OPENMP
    #include "omp.h"
#endif

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#include "Primes.h"
#include "MTRand.h"

#include "MathUtil.h"
using namespace MathUtil;

namespace Sampling {

    /******************************** Sampling ********************************/
    extern std::ifstream piFin;
    extern std::vector < std::vector < double > > sobol_points(unsigned N, unsigned D);
    extern double *single_sobol_points(unsigned N, unsigned D);

    extern std::vector < std::vector < double > > faureSampling(int Nd, int Ns);
    extern std::vector<int> changeBase(double num, double base, int numDigits);
    extern double corputBase(double base, double number);

    extern std::vector < std::vector < double > > latinHyperCube_Random(int numVars, int numSamples, MTRand& mt);
    extern std::vector < std::vector < double > > descriptiveSampling_Random(int numVars, int numSamples, MTRand& mt);

    extern std::vector < std::vector < double > > hammersleySampling(int Nd, int Ns);
    extern std::vector < std::vector < double > > haltonSampling(int Nd, int Ns);
    extern std::vector < std::vector < double > > faureSampling(int Nd, int Ns);

    extern double piNumber(int Ns);
    extern std::string toLower(std::string str);
    extern std::string toUpper(std::string str);
    extern std::vector<std::string> permuteCharacters(std::string topermute);
    extern std::string changeBase(std::string Base, int number);

    /******************************** End Sampling ********************************/
};

#endif /* SAMPLING_H_ */
