/*
 * MathUtil.h
 *
 *  Created on: April 27, 2017
 *      Author: aliviaj
 */

#ifndef MATHUTIL_H_
#define MATHUTIL_H_

#ifndef _OPENMP
    #include "omp.h"
#endif

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

namespace MathUtil {

    /******************************** Math ********************************/
    extern double sigMoid(double v);
    extern int factorial(int n);
    extern int combination(int n, int r);
    extern void twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A);
    extern double unitStep(double X);
    extern std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B);
    extern double expm (double p, double ak);
    extern double series(int m, int id);
    extern double piNumber(int sampleNumber);
    extern std::vector<double> decToBin(double n, int numDigits);

    /******************************** End Math ********************************/
};

#endif /* MATHUTIL_H_ */