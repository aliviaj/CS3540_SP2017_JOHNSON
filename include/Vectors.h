/*
 * Vectors.h
 *
 *  Created on: April 26, 2017
 *      Author: aliviaj
 */

#ifndef VECTORS_H_
#define VECTORS_H_

#ifndef _OPENMP
    #include "omp.h"
#endif

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

namespace Vectors {

    /******************************** Vector ********************************/
    extern void printVector(std::vector < std::vector < double > >& v, std::string title, int precision = 2);
    extern void printVector(std::vector < std::vector < std::vector < double > > >& v, std::string title, int precision = 2);
    /******************************** End Vector ********************************/
};

#endif /* VECTORS_H_ */
