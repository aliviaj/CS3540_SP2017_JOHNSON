/*
 * CommandLine.h
 *
 *  Created on: April 27, 2017
 *      Author: aliviaj
 */

#ifndef COMMANDLINE_H_
#define COMMANDLINE_H_

#ifndef _OPENMP
    #include "omp.h"
#endif

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#include "anyoption.h"

namespace CommandLine {

    /******************************** Command Line ********************************/
    extern void setUsage(AnyOption* opt);
    extern void setOptions(AnyOption* opt);
    /******************************** EndCommand Line ********************************/
};

#endif /* COMMANDLINE_H_ */