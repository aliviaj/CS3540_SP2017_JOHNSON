/*
 * System.h
 *
 *  Created on: April 27, 2017
 *      Author: aliviaj
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#ifndef _OPENMP
    #include "omp.h"
#endif

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#include "Bus.h"
#include "Generator.h"
#include "Line.h"

#include "StringUtil.h"
using namespace StringUtil;

namespace System {

	/******************************** System ********************************/
    extern void loadSystemData(double& pLoad, double& qLoad, int& nb, int& nt, std::string curSystem, std::vector<Generator>& gens, std::vector<Line>& lines, std::vector<Bus>& Buses);

    /******************************** End System ********************************/
};

#endif /* SYSTEM_H_ */