/*
 * StringUtil.h
 *
 *  Created on: April 27, 2017
 *      Author: aliviaj
 */

#ifndef STRINGUTIL_H_
#define STRINGUTIL_H_

#ifndef _OPENMP
    #include "omp.h"
#endif

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

namespace StringUtil {

	/******************************** String ********************************/
    extern std::string vectorToString(std::vector<double> v);
    extern std::string vectorToString(std::vector<int> v);
    extern std::string arrayToString(int* v, int size);
    extern void tokenizeString(std::string str,std::vector<std::string>& tokens,const std::string& delimiter );
    /******************************** End String ********************************/
};

#endif /* STRINGUTIL_H_ */