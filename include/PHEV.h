/*
 * PHEV.h
 *
 *  Created on: April 27, 2017
 *      Author: aliviaj
 */

#ifndef PHEV_H_
#define PHEV_H_

#ifndef _OPENMP
    #include "omp.h"
#endif

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#include "MTRand.h"
#include "RandomNumbers.h"
#include "defs.h"

#include "MathUtil.h"
using namespace MathUtil;
#include "Pruning.h"
using namespace Pruning;

namespace PHEV {

    /******************************** PHEV ********************************/
    extern void calculatePHEVLoad(
            double penetrationLevel, double rho,
            int totalVehicles, int numBuses,
            std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT PHEV_PLACEMENT = pruning::PP_EVEN_ALL_BUSES);

    /******************************** End PHEV ********************************/
};

#endif /* PHEV_H_ */