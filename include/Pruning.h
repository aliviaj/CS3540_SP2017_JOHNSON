/*
 * Pruning.h
 *
 *  Created on: April 27, 2017
 *      Author: aliviaj
 */

#ifndef PRUNING_H_
#define PRUNING_H_

#ifndef _OPENMP
    #include "omp.h"
#endif

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#include "defs.h"

#include "Sampling.h"
using namespace Sampling;

namespace Pruning {

    extern std::string getBooleanString             (bool value);
    extern std::string getPruningMethodString	 	(pruning::PRUNING_METHOD 		pm);
    extern std::string getClassificationMethodString(pruning::CLASSIFICATION_METHOD cm);
    extern std::string getSamplingMethodString		(pruning::SAMPLING_METHOD 		ps);
    extern std::string getPHEVPlacementString		(pruning::PHEV_PLACEMENT  		pp);
    extern std::string getStoppingMethodString		(pruning::STOPPING_METHOD 		sm);
    extern std::string getPruningObjString		    (pruning::PRUNING_OBJECTIVE 	po);
    
    extern pruning::PRUNING_METHOD        getPruningMethod		  (std::string s);
    extern pruning::CLASSIFICATION_METHOD getClassificationMethod (std::string s);
    extern pruning::SAMPLING_METHOD       getSamplingMethod		  (std::string s);
    extern pruning::PHEV_PLACEMENT        getPHEVPlacement		  (std::string s);
    extern pruning::STOPPING_METHOD       getStoppingMethod		  (std::string s);
    extern pruning::PRUNING_OBJECTIVE     getPruningObj           (std::string s);
};

#endif /* PRUNING_H_ */