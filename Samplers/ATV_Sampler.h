/*
 * LHS_Sampler.h
 *
 *  Sampling with Antithetic Variates
 */

#ifndef ATV_SAMPLER_H_
#define ATV_SAMPLER_H_

#include <vector>

#include "MCS_Sampler.h"

#include "PHEV.h"
using namespace PHEV;

class ATV_Sampler: public MCS_Sampler {
    public:
        ATV_Sampler();
        virtual ~ATV_Sampler();

        virtual void run(MTRand& mt);
};

#endif /* LHS_SAMPLER_H_ */
