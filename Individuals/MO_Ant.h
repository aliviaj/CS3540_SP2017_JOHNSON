/*
 * MO_Ant.h
 *
 *  Created on: Mar 29, 2011
 *      Author: rgreen
 */

#ifndef MO_ANT_H_
#define MO_ANT_H_

#include <vector>

class MO_Ant {
	public:
		MO_Ant();
		virtual ~MO_Ant();

			std::vector < int >    				pos;
			std::vector < std::vector < int > >    pBest;
			long double 		fitness;
			int 				startingPos;
			int					origStartingPos;
};

#endif /* MO_ANT_H_ */
