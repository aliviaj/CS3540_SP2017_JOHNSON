/*
 * Chromosome.h
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#ifndef CHROMOSOME_H_
#define CHROMOSOME_H_

#include <string>
#include <vector>
#include "Individual.h"

class Chromosome {
	public:
		Chromosome();
		Chromosome(int size);
		virtual ~Chromosome();
		std::string toString();


		std::vector<int> pos;
		double fitness;
};

#endif /* CHROMOSOME_H_ */
